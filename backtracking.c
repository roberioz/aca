//Alunos
//Rob�rio Pereira da Silva - 1920660
//Pedro Felipe S. Fernandes - 1920285

#include <stdio.h>
#include <stdlib.h>

int tabuleiro[8][8] = {{0, 0, 0, 0, 0, 0, 0, 0},
                       {0, 0, 0, 0, 0, 0, 0, 0},
                       {0, 0, 0, 0, 0, 0, 0, 0},
                       {0, 0, 0, 0, 0, 0, 0, 0},
                       {0, 0, 0, 0, 0, 0, 0, 0},
                       {0, 0, 0, 0, 0, 0, 0, 0},
                       {0, 0, 0, 0, 0, 0, 0, 0},
                       {0, 0, 0, 0, 0, 0, 0, 0}};

int movimentos[2][8] = {{2, 2, -2, -2, 1, 1, -1, -1},
                        {1, -1, 1, -1, 2, -2, 2, -2}};

typedef struct No No;
typedef struct coordenada coordenadas;

struct coordenada
{
    int x;
    int y;
};
struct No {
    int tamanho;
    coordenadas *caminho;
    struct No *prox;
};

No *listaInvalidos;

No *novoNo(int tamanho, coordenadas *vetor) {
    No *novo = (No*)malloc(sizeof(No));
    novo->tamanho = tamanho;
    novo->caminho = vetor;
    novo->prox = NULL;

    return novo;
}

void inserirListaInvalidos(int tamanho, coordenadas vetor[]) {
    No *novo = novoNo(tamanho, vetor);
    if(listaInvalidos == NULL) {
        listaInvalidos = novo;
    } else {
        novo->prox= listaInvalidos;
        listaInvalidos = novo;
    }
}   

int inseridoNoCaminho(coordenadas *caminhoPercorrido, coordenadas coord, int coordenadasPercorridas) {
    int i;
    for (i = 0; i < coordenadasPercorridas; i++) {
        if (caminhoPercorrido[i].x == coord.x && caminhoPercorrido[i].y == coord.y) {
            return 1;
        }
    }
    return 0;
}

int invalido(coordenadas *vetor, int coordenadasPercorridas, coordenadas coord) {
    No *aux = listaInvalidos;                    
    int contador = coordenadasPercorridas - 1;
    while(aux != NULL) {
            if(aux->caminho[coordenadasPercorridas].x == coord.x && aux->caminho[coordenadasPercorridas].y == coord.y)  {
                contador = coordenadasPercorridas - 1;
                int i;
                for(i = contador; i >= 0; i--) {
                    if(aux->caminho[i].x == vetor[i].x && aux->caminho[i].y == vetor[i].y) {
                        contador--;
                    } else break;
                    if(contador == 0) {
                        return 1;
                    } 
                }
            } 
        aux = aux->prox;
    }
    return 0;
}

void *copiar(coordenadas *vetor, coordenadas *vetor1, int tamanho) { 
    int i;
    for(i = 0; i < tamanho; i ++) {
        vetor1[i].x = vetor[i].x;
        vetor1[i].y = vetor[i].y;

    }

}

int valido(coordenadas *caminhoPercorrido, int coordenadasPercorridas, coordenadas coord) {
    if(inseridoNoCaminho(caminhoPercorrido, coord, coordenadasPercorridas)) {
        return 0;
    } else if (coord.x < 0 || coord.x > 7 || coord.y < 0 || coord.y > 7) {
        return 0;
    } else if(coordenadasPercorridas<64 && invalido(caminhoPercorrido, coordenadasPercorridas, coord)) {
        return 0;

    }
    return 1;
}

void printCaminhoPercorrido(coordenadas *caminhoPercorrido, int coordenadasPercorridas) {
    int i;
    printf("\n\n");
    for( i = 0 ; i < coordenadasPercorridas ; i++) {
        printf(" \n|%i,%i|", caminhoPercorrido[i].x, caminhoPercorrido[i].y);
    }
}

void acharCaminho(coordenadas *caminhoPercorrido, int coordenadasPercorridas) {
    int i,j;
    printCaminhoPercorrido(caminhoPercorrido, coordenadasPercorridas);
    coordenadas movimento;
    movimento.x = -1;
    movimento.y = -1;
    for(i = 0; i < 8; i++) {
        coordenadas mov;
        mov.x = movimentos[0][i] + caminhoPercorrido[coordenadasPercorridas - 1].x;
        mov.y = movimentos[1][i] + caminhoPercorrido[coordenadasPercorridas - 1].y;
        if (valido(caminhoPercorrido, coordenadasPercorridas, mov)) {
            movimento.x = movimentos[0][i] + caminhoPercorrido[coordenadasPercorridas - 1].x;
            movimento.y = movimentos[1][i] + caminhoPercorrido[coordenadasPercorridas - 1].y;
            break;
        }
    }
    if(movimento.x != -1 && movimento.y != -1) {
        caminhoPercorrido[coordenadasPercorridas] = movimento;
        tabuleiro[caminhoPercorrido[coordenadasPercorridas].x][caminhoPercorrido[coordenadasPercorridas].y] = -1;
        coordenadasPercorridas++;
        acharCaminho(caminhoPercorrido, coordenadasPercorridas);
    } else if(coordenadasPercorridas <= 63 && coordenadasPercorridas>=1) {
        tabuleiro[caminhoPercorrido[coordenadasPercorridas - 1].x][caminhoPercorrido[coordenadasPercorridas - 1].y] = 0;
        inserirListaInvalidos(coordenadasPercorridas, caminhoPercorrido);
        coordenadasPercorridas--;
        coordenadas *caminho = calloc(63, sizeof(coordenadas));
        copiar(caminhoPercorrido, caminho, coordenadasPercorridas);
        acharCaminho(caminho, coordenadasPercorridas);
    } else {
        if(coordenadasPercorridas == 64) {
            printf("\nCaminho encontrado!");
        } else {
            printf("\nN�o existe um caminho valido!");
        }
    }
}


int main() {
    int i;
    coordenadas *caminhoPercorrido = calloc(64, sizeof(coordenadas));
    coordenadas inicio;

    //Coordenada inicial
    caminhoPercorrido[0].x = 0; 
    caminhoPercorrido[0].y = 4;
    //-----------------

    inserirListaInvalidos(1, caminhoPercorrido);
    tabuleiro[caminhoPercorrido[0].x][caminhoPercorrido[0].y] = -1;
    acharCaminho(caminhoPercorrido, 1);

    printf("\n Coordenadas encontradas: \n");
    for(i = 0; i< 64;i ++) {
        printf("%i %i ,", caminhoPercorrido[i].x, caminhoPercorrido[i].y);
    }
}
