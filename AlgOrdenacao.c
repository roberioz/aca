#include <time.h>
#include <stdlib.h>
#include <stdio.h>

int quickTrocas = 0, selectionTrocas = 0, bubbleTrocas = 0, mergeTrocas = 0; 
int qckComp = 0, selectionComp = 0, bubbleComp = 0, mergeComp = 0; 

//Rob�rio Pereira da Silva - 1920660
//Pedro Felipe de Souza Fernandes - 1920285
void bubbleSort(int vetor[], int size) {
	int i, j, aux;
	for (j = 0; j < size - 1; j++) {
		for (i = 0; i < size - 1; i++) {
			bubbleComp++; 
			if (vetor[i] > vetor[i + 1]) {
				aux = vetor[i];
				vetor[i] = vetor[i + 1];
				vetor[i + 1] = aux;

				bubbleTrocas++; 
			}
		}
	}
}

void selectionSort(int vetor[], int size) {
	int i, j, menorN, menorIndice;
	for (j = 0; j < size; j++) {
		for (i = j; i < size; i++) {
			if (i == j) {
				menorN = vetor[i];
				menorIndice = i;
			}
			selectionComp++;
			if (vetor[i] < menorN) {
				menorN = vetor[i];
				menorIndice = i;
			}
		}
		if (i != j) {
			selectionTrocas++;
			trocar(vetor, menorIndice, j);
		}
	}
}


int trocar(int vetor[], int x, int y) {
    int aux = vetor[y];
    vetor[y] = vetor[x];
    vetor[x] = aux;
    return 1;
}

void organizar(int vetor[], int inicio, int meio, int fim) {
    int *vetorAuxiliar = calloc(fim + 1, sizeof(int));
	int x, y, i;
	int atual;
	atual = inicio;
	x = inicio;
	y = meio + 1;
	while (x <= meio && y <= fim) {
		mergeComp++;
		if (vetor[x] <= vetor[y]) {
			mergeTrocas++;
			vetorAuxiliar[atual] = vetor[x];
			x++;
		} else {
			vetorAuxiliar[atual] = vetor[y];
			y++;
		}
		atual++;
	}
	for (i = atual; i <= fim; i++) {
		if (x > meio) {
			vetorAuxiliar[atual] = vetor[y];
			y++;
		} else {
			vetorAuxiliar[atual] = vetor[x];
			x++;
		}
		atual++;
	}
	for (i = inicio; i <= fim; i++) {
		vetor[i] = vetorAuxiliar[i];
	}
    free(vetorAuxiliar);
}

void mergeSort(int vetor[], int inicio, int fim, int size) {
    if(inicio<fim) {
        int meio = (inicio+fim)/2;
        mergeSort(vetor, inicio, meio, size);
        mergeSort(vetor, meio+1, fim, size);
        organizar(vetor, inicio, meio, fim);
    }
}

int pivo(int vetor[],int pivo, int x, int y) {
    do {
        while(vetor[x] <= vetor[pivo])  {
			qckComp++;
            x++;
        }
        while(vetor[y] > vetor[pivo]) {
			qckComp++;
            y--;
        }
        if(x<y) {
            quickTrocas++; 
			trocar(vetor,x,y);
        }
    }while(x<y);
    quickTrocas++;
	trocar(vetor,pivo,y);
    return y;
}

void quick(int vetor[], int inicio, int fim) {
    int aux;
    if (inicio < fim) {
        aux = pivo(vetor,inicio,inicio+1,fim);
        quick(vetor, inicio, aux - 1);
        quick(vetor, aux + 1, fim);
    }
}



int main() {
    long long int tamanho = 0;
    int op = 0;
    while(op==0) {
    	printf("Informe a quantidade de elementos: \n1. para 5!\t\n2. para 10!\t\n3. para 15!\n");
   		scanf("%d", &op);
    	switch(op) {
			case 1:
				tamanho = 120;
				break;
			case 2: 
				tamanho = 3628800;
				break;
			case 3:
				tamanho = 1307674368000;
				break;
			default:
				op = 0;
				break;
		}
	}
    int *vetor = calloc(tamanho, sizeof(int));
    int i;
	for(i = 0; i < tamanho; i++) {
		vetor[i] = rand();
	}
    
    clock_t inicio, fim;
	printf("\n***** mergeSort *****");
	int *vetorMerge = calloc(tamanho, sizeof(int));
	vetorMerge = vetor;
	inicio = clock();
    mergeSort(vetorMerge, 0, tamanho-1, tamanho);
    fim = clock();
    printf("\nTempo gasto na ordenacao: %fs\n",(float)(fim - inicio)/CLOCKS_PER_SEC);
    printf("Quantidade de trocas: %d\n",mergeTrocas);
    printf("Quantidade de comparacoes: %d\n",mergeComp);

	printf("***** quickSort *****\n");
	int *vetorQuick = calloc(tamanho, sizeof(int));
	vetorQuick = vetor;
	inicio = clock();
    quick(vetorQuick ,0, tamanho-1);
    fim = clock();
    printf("\nTempo gasto na ordenacao: %fs\n",(float)(fim - inicio)/CLOCKS_PER_SEC);
    printf("Quantidade de trocas: %d\n",quickTrocas);
    printf("Quantidade de comparacoes: %d\n",qckComp);

	printf("\n***** selectionSort *****");
	int *vetorSelection = calloc(tamanho, sizeof(int));
	vetorSelection = vetor;
	inicio = clock();
    selectionSort(vetorSelection,tamanho);
    fim = clock();
    printf("\nTempo gasto na ordenacao: %fs\n",(float)(fim - inicio)/CLOCKS_PER_SEC);
    printf("Quantidade de trocas: %d\n",selectionTrocas);
    printf("Quantidade de comparacoes: %d\n",selectionComp);

	printf("\n***** bubbleSort *****");
	int *vetorBubble = calloc(tamanho, sizeof(int));
	vetorBubble = vetor;
	inicio = clock();
    bubbleSort(vetorBubble,tamanho);
    fim = clock();
    printf("\nTempo gasto na ordenacao: %fs\n",(float)(fim - inicio)/CLOCKS_PER_SEC);
    printf("Quantidade de trocas: %d\n",bubbleTrocas);
    printf("Quantidade de comparacoes : %d\n",bubbleComp);

	printf("\n Aperte alguma tecla para imprimir o vetor ordenado");
	system("pause");
    for(i = 0; i < tamanho; i++) 
    {
        printf("%d\n", vetor[i]);
    }     
    free(vetor);
}
