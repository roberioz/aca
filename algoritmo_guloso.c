//Alunos
//Rob�rio Pereira da Silva - 1920660
//Pedro Felipe S. Fernandes - 1920285

#include <stdio.h>
#include <stdlib.h>

typedef struct grafo Grafo;
typedef struct No No;

struct grafo {
    char vertice;
};

struct No {
    Grafo *grafo;
    int custoCaminho;
    struct No *prox;
};

No *caminhoMinimo;
int matrizAdj[16][16];
Grafo *vetorGrafos[16];

Grafo *alocarNovo(char vertice) {
    Grafo *grafo = malloc(sizeof(Grafo));
    grafo->vertice = vertice;
    return grafo;
}

No *alocarNovoNo(int custoCaminho, Grafo *vertice) {
    No *novo = (No *)malloc(sizeof(No));
    novo->custoCaminho = custoCaminho;
    novo->grafo = vertice;
    novo->prox = NULL;

    return novo;
}

No *inserir(No *novo) {
    No *aux = caminhoMinimo;
    while (aux->prox != NULL) {
        aux = aux->prox;
    }
    aux->prox = novo;
    return novo;
}

int estaContido(Grafo *grafo) {
    No *aux = caminhoMinimo;
    while (aux != NULL) {
        if (aux->grafo == grafo) {
            return 1;
        }
        aux = aux->prox;
    }
    return 0;
}

void caminhoGuloso(int atual, int final, int custoCaminho) {
    if (atual ==  final) {
        printf("Custo: %i\n", custoCaminho);
        return;
    }
    int menorCusto = -1;
    int indiceProximo = -1;
    int i;
    for(i = 0; i < 16; i++) {
        if(final == i && matrizAdj[atual][i] != -1) {
            menorCusto = matrizAdj[atual][i];
            indiceProximo = i;
            break;
        } else if((menorCusto > matrizAdj[atual][i] || menorCusto == -1) && matrizAdj[atual][i] != -1 && !estaContido(vetorGrafos[i])) {
            menorCusto = matrizAdj[atual][i];
            indiceProximo = i;
        }
    }
    if(indiceProximo == -1) {
        return;
    }

    custoCaminho += menorCusto;

    No *novo = alocarNovoNo(custoCaminho, vetorGrafos[indiceProximo]);
    inserir(novo);        
    
    caminhoGuloso(indiceProximo, final, custoCaminho);
}

int main() {
    int j = 0;
    int x, y;
    for (x = 0; x < 16; x++) {
        for (y = 0; y < 16; y++) {
            matrizAdj[x][y] = -1;
        }
    }      
    matrizAdj[0][2] = 5;
    matrizAdj[0][3] = 4;
    matrizAdj[1][4] = 3;
    matrizAdj[1][3] = 7;
    matrizAdj[2][5] = 2;
    matrizAdj[2][6] = 1;
    matrizAdj[3][6] = 1;
    matrizAdj[3][7] = 2;
    matrizAdj[4][7] = 5;
    matrizAdj[4][8] = 4;
    matrizAdj[5][9] = 3;
    matrizAdj[6][9] = 3;
    matrizAdj[6][10] = 4;
    matrizAdj[7][10] = 2;
    matrizAdj[7][11] = 2;
    matrizAdj[8][11] = 2;
    matrizAdj[9][12] = 5;
    matrizAdj[10][12] = 2;
    matrizAdj[10][13] = 8;
    matrizAdj[11][13] = 4;
    matrizAdj[12][15] = 2;
    matrizAdj[13][15] = 1;
    matrizAdj[14][0] = 1;
    matrizAdj[14][1] = 0;

    char i;
    for (i = 99; i < 115; i++) {
        vetorGrafos[j] = alocarNovo(i);
        printf("%c",i);
        printf("\n");
        j++;
    }

    char pc[2];
    printf("\nInforme o caminho a ser percorrido, ex: 'qr' -> ");
    scanf("%s", &pc);
    int partida;
    int chegada;

    for (j = 0; j < 16; j++) {
        if (vetorGrafos[j]->vertice == pc[1]) {
            chegada = j;
        }
        if (vetorGrafos[j]->vertice == pc[0]) {
            partida = j;
        }
    }
    caminhoMinimo = alocarNovoNo(0, vetorGrafos[partida]);
    caminhoGuloso(partida, chegada, 0);
    No *aux = caminhoMinimo;
    printf("Caminho: ");
    while(aux != NULL) {
        printf("%c ", aux->grafo->vertice);
        aux = aux->prox;
    }
}
