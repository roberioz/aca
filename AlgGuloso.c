#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>

//Rob�rio Pereira da Silva - 1920660
//Pedro Felipe de Souza Fernandes - 1920285
int main()
{    
	int vetorTempoInicial[11] = {1, 3, 0, 5, 3, 5, 6,  8,  8,  2, 12};
	int vetorTempoFinal[11] = {4, 5, 6, 7, 9, 9, 10, 11, 12, 14, 16};
	
    int maiorVetorTempoInicialIndex = 0;
    int menorVetorTempoFinalIndex;
    int tarefas[5];
    
    int i, j = 0;
    bool acabar = false;
    
    int menorValue= 0;
    for(i = 0; i < 11; i++) {
        if(vetorTempoFinal[menorValue] > vetorTempoFinal[i]) {
            menorValue = i;
        }
    }
    
    menorVetorTempoFinalIndex = menorValue;
    int menorFinalValue = vetorTempoFinal[menorVetorTempoFinalIndex];
    while(!acabar){
    	
        if(j == 0){
            tarefas[j] =menorVetorTempoFinalIndex+1;
        }
        int maiorVetorTempoInicio = 0;
    	for(i = 0; i < 11; i++){
        	if(vetorTempoInicial[i] > vetorTempoFinal[menorVetorTempoFinalIndex]) {
            	if(vetorTempoInicial[maiorVetorTempoInicio ] > vetorTempoInicial[i] || maiorVetorTempoInicio  == 0) {
               		maiorVetorTempoInicio  = i;
            	}	
        	}
    	}
    	maiorVetorTempoInicialIndex = maiorVetorTempoInicio ;
        menorVetorTempoFinalIndex = maiorVetorTempoInicialIndex;
        menorFinalValue = vetorTempoFinal[menorVetorTempoFinalIndex];
        j++;
        tarefas[j] = menorVetorTempoFinalIndex+1;
        
   		for(i = 0; i < 11;  i++) {
        	if(vetorTempoInicial[i] > vetorTempoFinal[menorVetorTempoFinalIndex]) {
            	acabar = false;
        	}else{
        		acabar = true;
			}
    	}	 
    }
    
    printf("----------Tarefas----------");
    
    for(i = 0; i <= j ; i++)
    {
    	printf("\nTarefas numero: : %i", tarefas[i]);
    }
    
    return 0;
}
